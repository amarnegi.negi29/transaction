<?php

namespace App\Http\Controllers;
use App\Models\Transaction as MT;

use Illuminate\Http\Request;
use App\Models\TransactionType;
use Illuminate\Support\Facades\Validator;
class Transaction extends Controller
{
    
    public function __construct() {

    }

    public function list(Request $request) {

       $transactions = MT::orderBy('id', 'desc')->get();
       $transaction_types = TransactionType::all();
       $types = [];
       foreach ($transaction_types as $type) {
           $types[$type->name] = $type->id;
       }
       return view('list', compact('transactions', 'types'));
    }


    public function add(Request $request) {
        $transaction_types = TransactionType::all();
        return view('add', compact('transaction_types'));
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'transaction_type_id' => 'required|in:1,2',
            'description' => 'required',
            'amount' => 'required|gt:0|digits_between:1,5',
        ]);
       
        if($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $latest_entry = MT::all()->last();
        $transaction_types = TransactionType::all();
        $types = [];
        foreach ($transaction_types as $type) {
            $types[$type->name] = $type->id;
        }
        
        $amount = $request->get('amount');
        if ($types['Debit'] == $request->get('transaction_type_id')) {
            $amount *= -1;
        }
        if ($latest_entry) {
            $new_balance = $latest_entry->balance + $amount;
        } else {
            $new_balance = $amount;
        }
        if ($new_balance < 0) {
            return redirect()->back()->withErrors(['balance' => 'Your amount is less than the balance amount'])->withInput();
        }
        $new_transaction = new MT($request->post());
        $new_transaction->balance = $new_balance;
        $new_transaction->save();
        return redirect()->route('listTransaction');
    }
}
