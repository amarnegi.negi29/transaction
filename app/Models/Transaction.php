<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //use HasFactory;
    protected $table = 'transaction';

    protected $fillable = ['transaction_type_id','description','amount', 'balance'];
}
