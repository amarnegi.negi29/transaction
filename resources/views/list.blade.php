<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>

<h2>Transactions</h2>
<a href="{{ route('addTransaction') }}">Add Transaction</a>
<table>
  <tr>
    <th>Date</th>
    <th>Description</th>
    <th>Credit</th>
    <th>Debit</th>
    <th>Running Balance</th>
  </tr>
  @foreach($transactions as $key => $transaction)
    <tr>
      <td>{{ date('m/d/Y', strtotime($transaction->created_at)) }}</td>
      <td>{{ $transaction->description }}</td>
      <td>{{ $transaction->transaction_type_id == $types['Credit'] ? $transaction->amount : '-' }}</td>
      <td>{{ $transaction->transaction_type_id == $types['Debit'] ? $transaction->amount : '-' }}</td>
      <td>{{ $transaction->balance }}</td>
    </tr>
  @endForeach
</table>

</body>
</html>

