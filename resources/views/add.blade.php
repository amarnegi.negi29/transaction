<!DOCTYPE HTML>
<html>

    <title>New Transaction</title>
    <style>
        .errors {
            color: red;
        }
    </style>
    <body>
        <h2>New Transaction</h2>
        <div class="errors">
            @if($errors->any())
                {!! implode('', $errors->all('<div>:message</div>')) !!}
            @endif
        </div>
        <form action="{{ route('storeTransaction') }}" method="post">
            {{ csrf_field() }}
            <div>
                <div class="col-md-6">Transaction Type</div>
                <div class="col-md-6">
                    <select name="transaction_type_id">
                    @foreach($transaction_types as $key => $type) 
                        <option value="{{ $type->id}}">{{ $type->name }}</option>
                    @endforeach 
                    </select>
                </div>
            </div>
            <div>
                <div class="col-md-6">amount</div>
                <div class="col-md-6">
                   <input type="number" value="{{ old('amount') }}" name="amount">
                </div>
            </div>
            <div>
                <div class="col-md-6">description</div>
                <div class="col-md-6">
                   <input type="text" value="{{ old('description') }}" name="description">
                </div>
            </div>
            <button><a href = "{{ route('listTransaction') }}">Cancel</a></button>
            <button type="submit">Save</button>
        </form>
    </body>
</html>